# <div align=center>WELCOME TO ARMY</div>
> 스마트폰과 함께하는 스마트한 기본군사훈련

군인이라면 누구나 거치는 곳, 훈련소.    
군의 장병 스마트폰 허용 확대 정책에 따라 이제 훈련병들도 스마트폰을 사용할 수 있게 되었다는 사실 알고 계셨나요?

고되고 힘든 훈련소 생활을 스마트하게 개선해주는 WELCOME TO ARMY와 함께라면, 훈련소 생활이 한층 더 스마트해집니다.

## 📌프로젝트 소개
매월 최소 1천여명에서 만여명의 훈련병이 민간인에서 군인으로 거듭나기 위해, 훈련소를 거쳐 갑니다.    
군대라는 조직을 처음 경험한 훈련병들에게 훈련소에서의 시간은 바깥세상과는 완전히 다른, 그야말로 혼란과 적응의 시간입니다.

한편, 이런 훈련병들을 올바르게 군인의 길로 이끌어줘야 하는 훈육관들에게도, 훈련 기간은 도전의 시간입니다.

훈육관 한 명당 수백명에 달하는 각기 다른 배경의 성인들을 관리하고, 훈육하는 것은 쉬운 일이 아닙니다.

WELCOME TO ARMY는 **훈련병에게는 쉽고 스마트한 훈련과정을, 훈육관에게는 효율적이고 편리한 훈련과정을 만들어주는 서비스**입니다.

## 💡주요 기능
### 👦훈련병
- 캘린더로 보는 훈련 일정
- 소대장 / 상담장교와의 대화 (상담/건의 게시판)
- 내 훈련 사진보기
- 내 훈련 결과 보기 (가감점 / 점수 보기)
- 모바일 교육 영상
- 훈련 공지사항 보기

### 😎훈육관 (조교, 상담장교 등)
- 병사 관리 전산화
- 훈련 일정 관리
- 설문 작성, 취합 전산화 및 자동화
- 상담 기록 전산화

## Wireframe
<table align="center">
	<tr>
		<td>
			<img style="width:450px;" src="/docs/wireframe/instructor/Trainee List Page.png">
		</td>
		<td>
			<img style="width:450px;" src="/docs/wireframe/instructor/Survey Create Page.png">
		</td>
	</tr>
	<tr>
		<td align="center">
			<b>훈육관 훈련병 관리 페이지</b>
		</td>
		<td align="center">
			<b>훈육관 조사전달 작성 페이지</b>
		</td>
	</tr>
</table>

## 기술 스택 (Technique Used) 
### Server(back-end)
- Spring Boot
### Front-end
- [Vuejs3](https://vuejs.org/)

## 컴퓨터 구성 / 필수 조건 안내 (Prerequisites
- ECMAScript 6 지원 브라우저 사용
- 권장: Google Chrome 버젼 77 이상

## 설치 안내 (Installation Process)
### Front-end
```bash
$ git clone https://github.com/osamhack2022/Web_WelcomeToTheArmy_WEGOHOME
$ cd frontend
$ npm install
$ npm run dev
```

## 프로젝트 사용법 (Getting Started)

## 😎 팀 정보 (TEAM INFORMATION)
<table align="center">
<tr>
	<th>Name</th>
	<th>Role</th>
	<th>Contact</th>
	<th>GITHUB</th>
</tr>
<tr>
	<td>백지오</td>
	<td>Project Manager, Front-end / UX Developer</td>
	<td>giopaik@naver.com</td>
	<td><a href="https://github.com/skyil7">skyil7</a></td>
</tr>
<tr>
	<td>이동규</td>
	<td>Back-end Development</td>
	<td>lsdong1003@naver.com</td>
	<td><a href="https://github.com/leedongkyu2019">leedongkyu2019</a></td>
</tr>
</table>


## 저작권 및 사용권 정보 (Copyleft / End User License)
This project is licensed under the terms of the MIT license.
